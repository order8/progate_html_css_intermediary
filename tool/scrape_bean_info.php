<?php
// コーヒー品種のデータをスクレイプする。
// 使用例 php ./scrape_bean_info.php

spl_autoload_register(function ($class_name) {
    include '../php/' . $class_name . '.php';
});

function get_dom($url)
{
    $curl = curl_init($url);
    $curl_options = [
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTPHEADER => ['Connection: close']
    ];
    curl_setopt_array($curl, $curl_options);
    $html = curl_exec($curl);
    curl_close($curl);

    $dom = new DomDocument();
    @$dom->loadHTML($html);
    return $dom;
}

$GreenCoffeeBeanInfo = new GreenCoffeeBeanInfo();

$url = 'https://varieties.worldcoffeeresearch.org/varieties';
$dom = get_dom($url);
$xpath = new DOMXPath($dom);
$bean_elements = $xpath->query('//a[@class="variety-tile variety-tile--grid-item"]');

$bean_records = [];

for($i = 0; $i < $bean_elements->length; $i++)
{
    $detail_page_url = $bean_elements->item($i)->getAttribute('href');
    $bean_element = $bean_elements->item($i)->childNodes;
    $variety_info = [];

    $variety_info[$GreenCoffeeBeanInfo::COLUMNS['COL_DETAIL_PAGE_URL']] = $detail_page_url;

    for($j = 0; $j < $bean_element->length; $j++)
    {
        $sub_elements = $bean_element->item($j);
        if(get_class($sub_elements) == 'DOMElement')
        {
            switch($sub_elements->getAttribute('class'))
            {
                case 'image':
                    $imgs = $sub_elements->getElementsByTagName('img');
                    if($imgs->length === 1)
                    {
                        $variety_info[$GreenCoffeeBeanInfo::COLUMNS['COL_IMAGE_URL']] = $imgs->item(0)->getAttribute('src');
                    }
                    break;
                case 'body':
                    $h3 = $sub_elements->getElementsByTagName('h3');
                    $p = $sub_elements->getElementsByTagName('p');
                    if($h3->length === 1 && $p->length === 1)
                    {
                        $variety_info[$GreenCoffeeBeanInfo::COLUMNS['COL_VARIETY_NAME']] = $h3->item(0)->nodeValue;
                        $variety_info[$GreenCoffeeBeanInfo::COLUMNS['COL_DESCRIPTION']] = $p->item(0)->nodeValue;
                    }
                    break;
                default:
                    echo 'something else';
            }
        }
    }
    $bean_records[] = $variety_info;
}

foreach($bean_records as $bean_record)
{
    $GreenCoffeeBeanInfo->insert($bean_record);
}
