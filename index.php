<?php
spl_autoload_register(function ($class_name)
{
    include 'php/' . $class_name . '.php';
});
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Coffee List</title>
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@100&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="./css/style.css">
</head>
<body>
  <header>
    <div class="header-logo">
      <h1>Coffee List</h1>
    </div>
    <h2>Check it out to learn how to separet logic and Template <a href="https://spin.atomicobject.com/2019/07/24/php-output-buffering/">click here to see the blog post</a></h2>
  </header>

  <?php
      $GreenCoffeeBeanInfo = new GreenCoffeeBeanInfo();
      $green_coffee_beans = $GreenCoffeeBeanInfo->select();

      $template = '
      <div class="bean">
        <div class="bean-icon">
          <a href="%s" target="_blank">
            <img src="%s">
            <p class="bean-name">%s</p>
            <p class="bean-description">%s</p>
          </a>
        </div>

      </div>
      ';

      echo '<div class="list">';
      foreach($green_coffee_beans as $bean)
      {
        echo sprintf($template, $bean['detail_page_url'], $bean['image_url'], $bean['variety_name'], $bean['description']);
      }
      echo '</div>';
  ?>
</body>
</html>

