<?php

class Database
{
    protected $connection;
    const COLUMNS = [];

    function __constructor($dsn = 'mysql:dbname=coffee_beans;host=127.0.0.1', $user = 'root', $password = 'c653e7ea218c9Fec89a32689c0e9ee04_')
    {
        try {
            $this->connection = new PDO($dsn, $user, $password);
        } catch (PDOException $e) {
            // echo '接続失敗: データベースエラー';
            echo '接続失敗： ' . $e->getMessage() . '<br>';
            exit();
        }
    }

    function insert($record)
    {
        $type = 'count';
        $id = $this->select($type);
        $id++;

        $columns = implode(',', $this::COLUMNS);
        $place_holder = [];
        foreach($this::COLUMNS as $key => $column)
        {
            $place_holder[$key] = ':' . $column;
        }
        $place_holder = implode(',', $place_holder);

        $insert = 'INSERT INTO %s (%s) VALUES (%s)';
        $insert = sprintf($insert, $this->table_name, $columns, $place_holder);

        $prepared = $this->connection->prepare($insert);

        foreach($this::COLUMNS as $column)
        {
            if($column == 'id')
            {
                $prepared->bindValue(':' . $column, $id);
            }
            else
            {
                $trimed = trim(preg_replace('!\s+!', ' ', $record[$column]));
                $prepared->bindValue(':' . $column, $trimed);
            }
        }

        $prepared->execute();
    }

    function select($type = 'all')
    {
        $base_sql = 'SELECT %s FROM %s';

        if($type == 'all')
        {
            $column = '*';
        }
        elseif($type == 'count')
        {
            $column = 'count(*)';
        }

        $select = sprintf($base_sql, $column, $this->table_name);
        $prepared = $this->connection->prepare($select);
        $prepared->execute();

        if($type == 'all')
        {
            $result = $prepared->fetchAll(PDO::FETCH_ASSOC);
        }
        elseif($type = 'count')
        {
            $result = $prepared->fetchColumn();
        }

        return $result;
    }
}