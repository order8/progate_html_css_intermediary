<?php

class GreenCoffeeBeanInfo extends Database
{
    public $table_name = 'green_coffee_bean_info';
    const COLUMNS = [
        'COL_ID' => 'id',
        'COL_VARIETY_NAME' => 'variety_name',
        'COL_DESCRIPTION' => 'description',
        'COL_IMAGE_URL' => 'image_url',
        'COL_DETAIL_PAGE_URL' => 'detail_page_url'
    ];

    function __construct()
    {
        parent::__constructor();
    }
}