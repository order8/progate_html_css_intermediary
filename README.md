# Scraper
* `tool/scrape_bean_info.php` はスクレイパー。サイトに必要な情報をスクレイプする。
* データベース `coffee_beans` が事前に存在することが実行条件。無いとエラー。

# SQL Dump
* `tool/coffee_beans_site.sql` は `coffee_beans` のSQL DUMP。
    * 実行すれば、データベースとテーブルが作られて、レコードもインサートされる。